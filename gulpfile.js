'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;

gulp.task('dev', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 63342,
        open: true,
        notify: false
    });

    gulp.watch('src/scss/*.scss', gulp.series('saas','minify-css')).on("change", reload);
    gulp.watch('src/js/*.js', gulp.series('uglify')).on("change", reload);
    gulp.watch('index.html').on("change", reload);

});

gulp.task('saas', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/scss'));
});

gulp.task('prefixer', function () {
    return gulp.src('src/scss/*.css')
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('src/scss'));
});

gulp.task('concat', function() {
    return gulp.src(['src/scss/*.css'])
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('src/scss'));
});

gulp.task('uglify', function(){
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/js'))
});


gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 63342,
        open: true,
        notify: false
    });

    gulp.watch("*.*").on("change", reload);
});



gulp.task('clean', function () {
    return gulp.src('dist/*', {read: false})
        .pipe(clean());
});

gulp.task('imagemin', () => {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});


gulp.task('minify-css', () => {
    return gulp.src('src/scss/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/css'));
});


// gulp.task('copy', function () {
//     return gulp.src('src/scss/*.css')
//         .pipe(gulp.dest('dist/css'));
// });

gulp.task('build', gulp.series('clean','saas','prefixer','uglify','concat','minify-css','imagemin'));

